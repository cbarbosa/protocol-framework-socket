/*
 * Template:
 * Essa classe implementa as características específicas
 * da entidade de protocolo MEIO
 */
package meio;

import send_receive_distr.Entidade;
import send_receive_distr.Estado;

public class Meio extends Entidade{
 public String ms;  
    // apontadores para as entidades relacionadas
    public int s;
    public int r;
    // apontadores para os estados da entidade
    public Estado _idle;
    public Meio(int _pl, int _ps, int _pr){
        super(_pl);
        s=_ps;
        r=_pr;
        _idle = new Meio_idle(this);
        est = _idle;
        System.out.println("Entidade Meio inicializada na porta "+_pl);
    }
}
