/*
 * Template:
 * Estado IDLE da entidade RECEIVER
 */
package receiver;

import send_receive_distr.Entidade;
import send_receive_distr.Estado;
import send_receive_distr.Evento;

public class Receiver_idle extends Estado{
    public Receiver_idle (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 9: // Evento ENTREGA
                // guarda msg
                ((Receiver)ent).ms = _ev.msg;
                // Evento de saida RECEBE
                    // entrega msg para o USUARIO
                    System.out.print("Mensagem: "+_ev.msg+"\n");
                // Evento de saida RESPONDE (entrega ack para o MEIO)
                    Evento e = new Evento(5,"responde","ack");
                    ent.msg.conecta("localhost", ((Receiver)ent).m); 
                    ent.msg.envia(e.toString());
                    ent.msg.termina();
                break;
            default: // evento inesperado
                System.out.println("RECEIVER descartou evento : "+_ev.code + " em IDLE");
        }
    }
}
