/*
 * Template: Classe para inicializar uma Entidade de protocolo
 * Inicializa a classe RECEIVER
 */
package receiver;

import send_receive_distr.Entidade;
import send_receive_distr.SThread;

public class Principal {
    String msg = null;
    Thread thread1, thread2;
    SThread sthread;
    SocketThread xthread; 
    Entidade s;
    public static void main(String args[]) {
        Principal p = new Principal();
        p.dispara(7002,7000); // porta local, porta do meio
    }
 
        public void dispara(int _pl, int _pr){
        s = new Receiver(_pl,_pr);
        // Inicia thread de tratamento de eventos
        sthread = new SThread(s);
        thread1 = new Thread(sthread);
        thread1.start();
        // Inicia threar de leitura do socket
        xthread = new SocketThread(s.msg, s);
        thread2 = new Thread(xthread);
        thread2.start();
    } 
}
