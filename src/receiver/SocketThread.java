/*
 * Sub-classe para desempacotar PDUs destinadas 
 * a ENTIDADE receiver.
 */
package receiver;

import send_receive_distr.Entidade;
import send_receive_distr.Evento;
import send_receive_distr.Msg;
import send_receive_distr.SimpleThread_r;

public class SocketThread extends SimpleThread_r {
    String n;
    String m;
    int code;
    public SocketThread(Msg _m, Entidade _u){
        super(_m, _u);
    }
    @Override
    public void desempacota(){
                    String [] split;
                    // Desempacota mensagem
                    split=tmp.split(",");
                    code= Integer.valueOf(split[0]);
                    n=split[1];
                    m=split[2];
                   // Cria Evento
                    Evento e = new Evento(code, n, m);
                   // Coloca no buffer da entidade
                   ent.colocaEvento(e); 
    }
}
