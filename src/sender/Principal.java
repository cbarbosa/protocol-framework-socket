/*
 *   Template: Classe para inicialização da entidade SENDER.
 * 
 *   Protocol Data Units (PDUs) do protocolo SEND/RECEIVER
 *      1: msg
 *      2: envia
 *      3: timeout
 *      4: confirma
 *      5: responde
 *      6: perde1
 *      7: perde2
 *      9: entrega
 *      10: recebe
 */
package sender;

import send_receive_distr.Entidade;
import send_receive_distr.Evento;
import send_receive_distr.SThread;

public class Principal {
    String msg = null;
    Thread thread1, thread2;
    SThread sthread;
    SocketThread xthread; 
    Entidade s;
    public static void main(String args[]) {
        Principal p = new Principal();
        p.dispara(7001,7000); // porta local, porta do meio
        p.inicia();
    }
 
   public void dispara(int _pl, int _pr){
        s = new Sender(_pl,_pr);
        // Inicia thread de tratamento de eventos
        sthread = new SThread(s);
        thread1 = new Thread(sthread);
        thread1.start();
        // Inicia thread de leitura do socket
        xthread = new SocketThread(s.msg, s);
        thread2 = new Thread(xthread);
        thread2.start();
    } 
   // Gera evento que dá inicio ao comportamento do protocolo SEND/RECEIVER
   public void inicia(){
                 // Le dado do usuário 
                 ((Sender)s).ms = ((Sender)s).le();
                 // gera evento MSG
                 ((Sender)s).colocaEvento(new Evento(1,"msg",((Sender)s).ms));      
    }
}
