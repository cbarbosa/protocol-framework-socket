/*
 * Template:
 * Estado SENDING da entidade SENDER
 */
package sender;

import send_receive_distr.Entidade;
import send_receive_distr.Estado;
import send_receive_distr.Evento;

public class Sender_sending extends Estado{
    public Sender_sending(Entidade _e){
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){ 
            case 3: // Evento TIMEOUT
                // Informa Timeout
                System.out.println("Timeout");
                // Evento de saida ENVIA (mensagem para o meio)
                Evento e = new Evento(2,"envia",((Sender)ent).ms);
                ent.msg.conecta("localhost", ((Sender)ent).m); 
                ent.msg.envia(e.toString());
                ent.msg.termina();
                // Dispara o timer
                ((Sender)ent).thread1 = new Thread(((Sender)ent).t1);
                ((Sender)ent).thread1.start();  
                break;
            case 4: // Evento CONFIRMA
                 // Para timer
                 ((Sender)ent).t1.paraTimer();                    
                 // Muda estado para IDLE
                 ((Sender)ent).est = ((Sender)ent)._idle;
                 // Le dado do usuário 
                 ((Sender)ent).ms = ((Sender)ent).le();
                 // gera evento MSG
                 ((Sender)ent).colocaEvento(new Evento(1,"msg",((Sender)ent).ms));                
            default:
                System.out.println("SENDER descartou evento : "+_ev.code + " em SENDING");
        }
    }
}
