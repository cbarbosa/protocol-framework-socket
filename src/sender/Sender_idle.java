/*
 * Template:
 * Estado IDLE da entidade SENDER
 */
package sender;

import send_receive_distr.Entidade;
import send_receive_distr.Estado;
import send_receive_distr.Evento;

public class Sender_idle extends Estado{
    public Sender_idle (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){ 
            case 1: // Evento MSG
                // muda estado para SENDING
                    ((Sender)ent).est=((Sender)ent)._sending;
                // Evento de saida ENVIA (mensagem para o MEIO)
                    Evento e = new Evento(2,"envia",((Sender)ent).ms);
                    ent.msg.conecta("localhost", ((Sender)ent).m); 
                    ent.msg.envia(e.toString());
                    ent.msg.termina();
                // dispara o timer
                    ((Sender)ent).thread1 = new Thread(((Sender)ent).t1);
                    ((Sender)ent).thread1.start();    
                break;                
            default: // evento inesperado
                System.out.println("SENDER descartou evento : "+_ev.code + " em IDLE");
        }
    }
}
